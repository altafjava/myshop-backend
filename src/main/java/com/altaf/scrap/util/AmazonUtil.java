package com.altaf.scrap.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AmazonUtil {

	private static List<String> keywords = new ArrayList<>();
	private static String[] keywordsArray = { "headphone", "earphone", "bass", "jack", "cable", "usb", "otg", "ring", "sticker", "bluetooth", "sweatproof", "back", "charger",
			"protector", "card", "case", "accessory", "micro", "accessories", "adapter", "kit", "game", "rpm", "rugged", "craft", "holder", "charging", "grip", "port", "mount",
			"wireless", "protection", "slip", "flip", "rapid", "wall", "power", "combo", "headset", "batteries", "warranty", "bud", "stereo", "pocket", "shock", "fiber",
			"magnetic", "pouch", "hard", "armor", "stand", "leather", "lens", "telephoto", "telescope", "effect", "cell", "macro", "guard", "dslr", "sdhc", "sdxc", "speed", "sd",
			"audio", "type", "device", "splitter", "duplicator", "angle", "gift", "polymer", "bank", "hybrid", "tempered", "glass", "screen", "edge", "scratch", "stick",
			"extendable", "wired", "printed", "logo", "watch", "band", "portable", "speaker", "radio", "multimedia", "laptop", "pen", "screw", "driver", "tool", "cleaner",
			"silicone", "surface", "pubg", "fan", "cooler", "electric" };

	static {
		keywords = Arrays.asList(keywordsArray);
	}

	public static boolean isMobileAccesory(String title, double finalPrice) {
		if (title.equals("Samsung Galaxy Smartwatch 46 MM(Silver),SM-R800NZSAINU")) {
			System.out.println("---");
			System.out.println(title);
		}
		boolean flag = keywords.parallelStream().anyMatch(title::contains) && finalPrice < 5000;
		if (keywords.parallelStream().anyMatch(title::contains)) {
			flag = true;
			// if (finalPrice < 5000)
			// flag = true;
			// else
			// flag = false;
		} else {
			if (finalPrice < 4000)
				flag = true;
			else
				flag = false;
		}
		return flag;
	}

}
