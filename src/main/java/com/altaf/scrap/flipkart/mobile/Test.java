package com.altaf.scrap.flipkart.mobile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Test {

	public static void main(String[] args) throws IOException {
		int count = 0;
		String baseUrl = "https://www.flipkart.com/mobiles/mi~brand/pr?sid=tyy%2C4io&page=";
		String url = baseUrl + 1;
		// Document document = Jsoup.connect(url).get();
		Document document = Jsoup.connect(url).header("Accept-Encoding", "gzip, deflate")
				.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0").maxBodySize(0).timeout(600000).get();
		Elements pageElement = document.getElementsByClass("_2zg3yZ");
		Elements spanElement = pageElement.get(0).getElementsByTag("span");
		String page = spanElement.text();
		int min = Integer.parseInt(page.substring(5, page.indexOf("of")).trim());
		int max = Integer.parseInt(page.substring(page.indexOf("of") + 2, page.indexOf("Next")).trim());
		System.out.println("min=" + min + "   max=" + max);
		getBox(document, count);
		// for (int i = min + 1; i <= max; i++) {
		for (int i = min + 1; i <= 1; i++) {
			url = baseUrl + i;
			// document = Jsoup.connect(url).get();
			document = Jsoup.connect(url).header("Accept-Encoding", "gzip, deflate")
					.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0").maxBodySize(0).timeout(600000).get();
			getBox(document, count);
		}
		System.err.println("count=" + count);
	}

	static public void getBox(Document document, int count) {
		// Elements elements = document.getElementsByClass("col-7-12");
		Elements elements = document.getElementsByClass("_31qSD5");
		for (Element element : elements) {
			String title = element.getElementsByClass("_3wU53n").text();
			System.out.println(title);
			String titleLink = element.select("a").first().attr("href");
			System.out.println(titleLink);
			String titleImageLink = element.select("img").first().attr("src");
			Element elm = element.select("img").first();
			Elements els = element.getElementsByClass("_1Nyybr _30XEf0");// .attr("src");
			System.out.println(titleImageLink);
			Elements properties = element.getElementsByClass("tVe95H");
			List<String> propertiesList = new ArrayList<>();
			for (Element propertyElement : properties) {
				propertiesList.add(propertyElement.text());
				System.out.println(propertyElement.text());
			}
			String discountedPrice = element.getElementsByClass("_1vC4OE _2rQ-NK").text();
			String originalPrice = element.getElementsByClass("_3auQ3N _2GcJzG").text();
			String discountPercentage = element.getElementsByClass("VGWI6T").text();
			System.out.println(originalPrice + "  -  " + discountedPrice + "  =  " + discountPercentage);
			String ratingAndReview = element.getElementsByClass("niH0FQ").text();
			System.out.println(ratingAndReview);
			System.out.println();
			count++;
		}
	}
}
