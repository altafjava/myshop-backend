package com.altaf.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.altaf.main.dao.MobileDao;
import com.altaf.main.dto.PageableMobile;

@Service
public class ProductService {

	@Autowired
	private MobileDao mobileDao;

	public PageableMobile searchProduct(String searchString, int currentPage) {
		return mobileDao.searchMobiles(searchString, currentPage);
	}
}
