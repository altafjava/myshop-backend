package com.altaf.scrap.dumper.flipkart.mobile;

import javax.annotation.PostConstruct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.altaf.scrap.service.CommonScrapService;

@Service
public class MiMobileDumper {

	private static final Logger LOGGER = LogManager.getLogger(MiMobileDumper.class);

	// minimum height/width=10/10
	// maximum height/width=4499/4499
	// q=1-100
	@Value("${willFlipkartDataSave}")
	private boolean willFlipkartDataSave;
	@Autowired
	private CommonScrapService commonScrapService;

	@PostConstruct
	public void scrapRealmeMobile() throws Exception {
		if (willFlipkartDataSave) {
			LOGGER.info("MiMobileDumper starting...");
			String endpoint = "https://www.flipkart.com/mobiles/mi~brand/pr?sid=tyy%2C4io";
			LOGGER.info("endpoint=" + endpoint);
			Document document = Jsoup.connect(endpoint).timeout(60000).get();
			boolean isSuccessfullyCompleted = commonScrapService.scrapFlipkartMobile(document, endpoint);
			if (isSuccessfullyCompleted)
				LOGGER.info("-------------------- MiMobileDumper Saved -----------------");
			else
				LOGGER.error("-------------------- MiMobileDumper Failed -----------------");
		}
	}
}
