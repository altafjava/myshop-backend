package com.altaf.main.controller;

import java.io.IOException;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MobileController {

	@Autowired
	private RestHighLevelClient restHighLevelClient;

	@GetMapping("/mobiles")
	public void mobiles() {
		SearchRequest searchRequest = new SearchRequest();
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.from(0);
		searchSourceBuilder.size(5);
		searchSourceBuilder.query(QueryBuilders.matchQuery("title", "redmi note"));
		// searchSourceBuilder.query(QueryBuilders.matchAllQuery());
		searchRequest.source(searchSourceBuilder);
		searchRequest.indices("products.phone");
		SearchResponse searchResponse = null;
		try {
			searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (searchResponse != null && searchResponse.getHits().getTotalHits().value > 0) {
			System.out.println("TotalHits=" + searchResponse.getHits().getTotalHits());
			for (SearchHit searchHit : searchResponse.getHits()) {
				System.out.println(searchHit.getSourceAsMap());
			}
		} else {
			System.out.println("no results found");
		}
	}

}
