package com.altaf.scrap.amazon.mobile;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MobilesScrapper {

	static int count = 0;

	public static void main(String[] args) throws IOException {
		System.out.println("starting....");
		String baseUrl = "https://www.amazon.in/s?rh=n%3A976419031%2Cn%3A%21976420031%2Cn%3A1389401031&page=";
		String url = baseUrl + 1;
		Document document = Jsoup.connect(url).get();
		Elements mainElements = document.getElementsByAttribute("data-result-rank");
		for (Element element : mainElements) {
			Elements sponsoredElements = element.select("h5");
			if (sponsoredElements.isEmpty()) {
				Elements titleElements = element.getElementsByTag("h2");
				String title = titleElements.text();
				Element titleLinkElement = element.select("a").first();
				String titleLink = titleLinkElement.attr("href");
				String titleImageLink = titleLinkElement.select("img").attr("src");
				System.out.println(title);
				System.out.println(titleLink);
				System.out.println(titleImageLink);
				String discountedPrice = element.getElementsByClass("a-size-base a-color-price s-price a-text-bold").text();
				String originalPrice = element.getElementsByClass("a-size-small a-color-secondary a-text-strike").text();
				String discountAmountAndPercentage = element.getElementsByClass("a-size-small a-color-price").text();
				System.out.println(originalPrice + " - " + discountAmountAndPercentage + " = " + discountedPrice);
				String rating = element.getElementsByClass("a-icon-star").text();
				String review = element.select("a").last().text();
				System.out.println(rating + "   " + review);
				count++;
			}
			System.out.println();
		}
		int maxPage = Integer.parseInt(document.getElementsByClass("pagnDisabled").text().trim());
		// for (int i = 2; i <= maxPage; i++) {
		for (int i = 2; i <= 5; i++) {
			url = baseUrl + i;
			document = Jsoup.connect(url).get();
			System.out.println(document.text());
			Elements dataIndexElements = document.getElementsByAttribute("data-index");
			for (Element element : dataIndexElements) {
				Elements sponseredElements = element.getElementsByClass("a-row a-spacing-micro");
				if (sponseredElements.isEmpty()) {
					Elements titleElements = element.getElementsByTag("h2");
					String title = titleElements.text();
					String titleLink = "https://amazon.in/" + titleElements.select("a").first().attr("href");
					System.out.println(title);// JBL C100SI In-Ear Headphones with Mic (Red)
					System.out.println(titleLink);// https://amazon.in//Redmi-Note-Cosmic-Purple-Storage/dp/B07X2KLKRT/ref=sr_1_52?qid=1581600006&s=electronics&smid=A23AODI1X2CEAE&sr=1-52
					String titleImageLink = element.getElementsByClass("a-section aok-relative s-image-fixed-height").select("img").first().attr("src");
					System.out.println(titleImageLink);// https://m.media-amazon.com/images/I/81+0t7kNmCL._AC_UY218_ML3_.jpg
					String reviewAndRating = element.getElementsByClass("a-row a-size-small").text();
					System.out.println(reviewAndRating);// 4.0 out of 5 stars 54,310
					String priceSymbol = element.getElementsByClass("a-price-symbol").text();
					String discountedPrice = element.getElementsByClass("a-price-whole").text();
					String originalPrice = element.getElementsByClass("a-price a-text-price").get(0).getElementsByClass("a-offscreen").text();
					System.out.print(priceSymbol + discountedPrice + ",  " + originalPrice + " ");// ₹10,499
					String priceDiscountDetails = element.getElementsByClass("a-row a-size-base a-color-base").text();
					if (priceDiscountDetails.contains("Save")) {
						String discountAndPercentage = priceDiscountDetails.substring(priceDiscountDetails.indexOf("Save"));
						System.out.println(discountAndPercentage);// ₹12,999, ₹15,999 Save ₹3,000 (19%)
					}
					String deliveryBy = element.getElementsByClass("a-row a-size-base a-color-secondary s-align-children-center").text();
					if (deliveryBy.contains("FREE")) {
						System.out.println(deliveryBy.substring(deliveryBy.indexOf("FREE")));// FREE Delivery by Amazon
					}
					System.out.println();
					count++;
				}
			}
		}
		System.out.println("count=" + count);
	}

}
