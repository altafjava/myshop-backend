package com.altaf.scrap;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CodeTriageTest {

	public static void main(String[] args) throws IOException {
		Document document = Jsoup.connect("https://www.codetriage.com/?language=Java").get();
		Elements elements = document.getElementsByClass("repo-item");
		for (Element element : elements) {
			String title = element.getElementsByClass("repo-item-title").text();
			String issues = element.getElementsByClass("repo-item-issues").text();
			String description = element.getElementsByClass("repo-item-description").text();
			String fullName = element.getElementsByClass("repo-item-full-name").text();
			System.out.println(title + " || " + issues + " || " + fullName + " || " + description);
		}
	}
}
//col-7-12
