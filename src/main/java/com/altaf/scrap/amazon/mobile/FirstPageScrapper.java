package com.altaf.scrap.amazon.mobile;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FirstPageScrapper {

	public static void main(String[] args) throws IOException {
		System.out.println("starting....");
		String url = "https://www.amazon.in/s?rh=n%3A976419031%2Cn%3A%21976420031%2Cn%3A1389401031&page=1";
		Document document = Jsoup.connect(url).get();
		int maxPage = Integer.parseInt(document.getElementsByClass("pagnDisabled").text().trim());
		Elements mainElements = document.getElementsByAttribute("data-result-rank");
		for (Element element : mainElements) {
			Elements sponsoredElements = element.select("h5");
			if (sponsoredElements.isEmpty()) {
				Elements titleElements = element.getElementsByTag("h2");
				String title = titleElements.text();
				Element titleLinkElement = element.select("a").first();
				String titleLink = titleLinkElement.attr("href");
				String titleImageLink = titleLinkElement.select("img").attr("src");
				System.out.println(title);
				System.out.println(titleLink);
				System.out.println(titleImageLink);
				String discountedPrice = element.getElementsByClass("a-size-base a-color-price s-price a-text-bold").text();
				String originalPrice = element.getElementsByClass("a-size-small a-color-secondary a-text-strike").text();
				String discountAmountAndPercentage = element.getElementsByClass("a-size-small a-color-price").text();
				System.out.println(originalPrice + " - " + discountAmountAndPercentage + " = " + discountedPrice);
				String rating = element.getElementsByClass("a-icon-star").text();
				String review = element.select("a").last().text();
				System.out.println(rating + "   " + review);
			}
			System.out.println();
		}
	}

}
