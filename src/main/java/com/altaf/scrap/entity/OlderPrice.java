package com.altaf.scrap.entity;

import java.util.Date;

public class OlderPrice {

	private Date date;
	private PriceObject priceObject;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public PriceObject getPriceObject() {
		return priceObject;
	}

	public void setPriceObject(PriceObject priceObject) {
		this.priceObject = priceObject;
	}

}
