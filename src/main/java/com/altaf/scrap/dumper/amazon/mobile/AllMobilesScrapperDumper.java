package com.altaf.scrap.dumper.amazon.mobile;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.altaf.scrap.entity.Mobile;
import com.altaf.scrap.repository.MobileRepository;

@Service
public class AllMobilesScrapperDumper {

	static int count = 0;

	@Value("${willAmazonDataSave}")
	private boolean willAmazonDataSave;
	@Autowired
	private MobileRepository mobileRepository;

	@PostConstruct
	public void scrapMobileAndAccessories() throws IOException {
		if (willAmazonDataSave) {
			// public static void main(String[] args) throws IOException {
			// if (true) {
			int milliseconds = 120000;
			System.out.println("starting....");
			String targetHostName = "https://amazon.in/";
			String baseUrl = "https://www.amazon.in/s?rh=n%3A976419031%2Cn%3A%21976420031%2Cn%3A1389401031&page=";
			String url = baseUrl + 1;
			Document document = Jsoup.connect(url).timeout(milliseconds).get();
			Elements mainElements = document.getElementsByAttribute("data-result-rank");
			System.out.println("page=" + 1);
			for (Element element : mainElements) {
				Elements sponsoredElements = element.select("h5");
				if (sponsoredElements.isEmpty()) {
					count++;
					Elements titleElements = element.getElementsByTag("h2");
					Mobile mobile = new Mobile();
					mobile.setCompanyName("amazon");
					String title = titleElements.text();
					System.out.println(title);
					mobile.setTitle(title);
					Element titleLinkElement = element.select("a").first();
					String titleUrl = targetHostName + "/" + titleLinkElement.attr("href");
					mobile.setUrl(titleUrl);
					String titleImageUrl = titleLinkElement.select("img").attr("src");
					mobile.setTitleImageLink(titleImageUrl);
					String finalPrice = element.getElementsByClass("a-size-base a-color-price s-price a-text-bold").text().trim().replaceAll(",", "");
					System.out.println(finalPrice);
					mobile.setDiscountAmount(Double.parseDouble(finalPrice.trim()));
					String mrp = element.getElementsByClass("a-size-small a-color-secondary a-text-strike").text().trim().replaceAll(",", "");
					// System.out.println(mrp);
					mobile.setMrp(Double.parseDouble(mrp.trim()));
					String discountAmountAndPercentage = element.getElementsByClass("a-size-small a-color-price").text();
					double discountPercentage = Double.parseDouble(
							discountAmountAndPercentage.substring(discountAmountAndPercentage.indexOf("(") + 1, discountAmountAndPercentage.indexOf("%")));
					// System.out.println(discountPercentage);
					mobile.setDiscountPercentage(discountPercentage);
					double discountAmount = Double
							.parseDouble(discountAmountAndPercentage.replaceAll(",", "").substring(0, discountAmountAndPercentage.indexOf("(") - 1).trim());
					// System.out.println(discountAmount);
					mobile.setDiscountAmount(discountAmount);
					double baseRating = 5;
					mobile.setBaseRating(baseRating);
					String rating = element.getElementsByClass("a-icon-star").text();
					double userRating = Double.parseDouble(rating.substring(0, rating.indexOf("out of")).trim());
					// System.out.println(userRating);
					mobile.setUserRating(userRating);
					String review = element.select("a").last().text();
					mobile.setUserReviewCount(Integer.parseInt(review.replaceAll(",", "")));
					// System.out.println();
					// saveMobile(mobile);
				}
			}
			int maxPage = Integer.parseInt(document.getElementsByClass("pagnDisabled").text().trim());
			System.out.println("maxPage=" + maxPage);
			for (int i = 2; i <= maxPage; i++) {
				// for (int i = 6; i <= 6; i++) {
				System.out.println("page=" + i);
				url = baseUrl + i;
				document = Jsoup.connect(url).timeout(milliseconds).get();
				Elements dataIndexElements = document.getElementsByAttribute("data-index");
				for (Element element : dataIndexElements) {
					Elements sponseredElements = element.getElementsByClass("a-row a-spacing-micro");
					if (sponseredElements.isEmpty()) {
						Mobile mobile = new Mobile();
						mobile.setCompanyName("amazon");
						Elements titleElements = element.getElementsByTag("h2");
						String title = titleElements.text();
						// System.out.println(title);
						mobile.setTitle(title); // JBL C100SI In-Ear Headphones with Mic (Red)
						String titleLink = targetHostName + titleElements.select("a").first().attr("href");
						mobile.setUrl(titleLink);// https://amazon.in//Redmi-Note-Cosmic-Purple-Storage/dp/B07X2KLKRT/ref=sr_1_52?qid=1581600006&s=electronics&smid=A23AODI1X2CEAE&sr=1-52
						String titleImageLink = element.getElementsByClass("a-section aok-relative s-image-fixed-height").select("img").first().attr("src");
						mobile.setTitleImageLink(titleImageLink);// https://m.media-amazon.com/images/I/81+0t7kNmCL._AC_UY218_ML3_.jpg
						Element ratingAndReviewElement = element.getElementsByClass("a-row a-size-small").first();
						if (ratingAndReviewElement != null) {
							Element ratingElement = ratingAndReviewElement.getElementsByTag("span").first();
							String userRatingString = ratingElement.getElementsByAttribute("aria-label").text();
							String[] ratingSplits = userRatingString.split("out of");
							double userRating = Double.parseDouble(ratingSplits[0].trim());// 4.0
							// System.out.println("userRating=" + userRating);
							mobile.setUserRating(userRating);
							double baseRating = Double.parseDouble(ratingSplits[1].substring(0, ratingSplits[1].indexOf("star")));
							// System.out.println(baseRating);
							mobile.setBaseRating(baseRating);// 5
							Element reviewElement = ratingAndReviewElement.getElementsByTag("span").last();
							int userReviewCount = Integer.parseInt(reviewElement.text().replaceAll(",", "").trim());
							// System.out.println(userReviewCount);
							mobile.setUserReviewCount(userReviewCount);// 54,310
						}
						String priceSymbol = element.getElementsByClass("a-price-symbol").text();
						mobile.setCurrency("INR");
						String finalPriceString = element.getElementsByClass("a-price-whole").text().replaceAll("[^0-9]", "").trim();
						if (finalPriceString.isEmpty()) {
							finalPriceString = element.getElementsByClass("a-color-price").text().replaceAll("[^0-9]", "").trim();
						}
						double finalPrice = Double.parseDouble(finalPriceString);
						// System.out.println(finalPrice);
						mobile.setFinalPrice(finalPrice);
						try {
							double mrp = Double.parseDouble(element.getElementsByClass("a-price a-text-price").first().getElementsByClass("a-offscreen").text()
									.replaceAll("[^0-9]", "").trim());
							// System.out.println(mrp);
							mobile.setMrp(mrp);
							double discountAmount = mrp - finalPrice;
							// System.out.println(discountAmount);
							mobile.setDiscountAmount(discountAmount);
							double discountPercentage = Math.round((discountAmount * 100) / mrp);
							// System.out.println(discountPercentage);
							mobile.setDiscountPercentage(discountPercentage);
						} catch (NullPointerException e) {
						}
						saveMobile(mobile);
					}
				}
			}
			System.out.println("------------- AllMobilesScrapperDumper Saved ---------");
		}
	}

	private void saveMobile(Mobile mobile) {
		mobileRepository.save(mobile);
	}
}
