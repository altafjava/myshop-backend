package com.altaf.scrap.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.altaf.scrap.entity.Mobile;
import com.altaf.scrap.entity.MobileBackup;

@Repository
// public interface MobileRepository extends MongoRepository<Mobile, String> {
public interface MobileBackupRepository extends CrudRepository<MobileBackup, String> {

	Page<MobileBackup> findBy(TextCriteria textCriteria, Pageable pageable);

	// find all matching documents and sort by relevance
	List<MobileBackup> findAllByOrderByScoreDesc(TextCriteria criteria);
}
