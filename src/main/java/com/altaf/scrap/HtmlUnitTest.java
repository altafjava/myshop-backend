package com.altaf.scrap;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.ErrorHandler;
import org.w3c.dom.NamedNodeMap;

import com.gargoylesoftware.css.parser.CSSException;
import com.gargoylesoftware.css.parser.CSSParseException;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.IncorrectnessListener;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HTMLParser;
import com.gargoylesoftware.htmlunit.html.HTMLParserListener;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;

public class HtmlUnitTest {

	static int count = 0;

	public static void main(String[] args) {
		// String baseUrl = "https://news.ycombinator.com/";
		String baseUrl = "https://www.flipkart.com/mobiles/mi~brand/pr?sid=tyy%2C4io&page=2";
//		WebClient client = new WebClient();
		WebClient client=new WebClient(BrowserVersion.FIREFOX_60);
		client.getOptions().setCssEnabled(false);
		client.getOptions().setThrowExceptionOnScriptError(false);
		client.getOptions().setThrowExceptionOnFailingStatusCode(false);
		try {
			FileOutputStream fos = new FileOutputStream("src/main/resources/b.xml");
			HtmlPage page = client.getPage(baseUrl);
			System.err.println("js="+client.getJavaScriptTimeout()+"   "+client.getOptions().isJavaScriptEnabled());
			client.waitForBackgroundJavaScript(3000);
			client.getOptions().setJavaScriptEnabled(true);
			System.err.println("js="+client.getJavaScriptTimeout()+"   "+client.getOptions().isJavaScriptEnabled());
			fos.write(page.asXml().getBytes());
			System.out.println("---------------DONE--------------------");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// public static void main(String[] args) throws IOException {
	// // String url = "https://www.flipkart.com/mobiles/mi~brand/pr?sid=tyy%2C4io&page=8";
	// String url = "https://www.flipkart.com/mobiles/mi~brand/pr?sid=tyy%2C4io&page=1";
	// // String url = "https://newyork.craigslist.org/search/moa?is_paid=all&search_distance_type=mi&query=iphone+6s";
	//// WebClient webClient = new WebClient();
	// WebClient webClient = new WebClient(BrowserVersion.CHROME);
	// Page page = webClient.getPage(url);
	// webClient.getOptions().setThrowExceptionOnScriptError(false);
	// webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
	// WebClientOptions webClientOptions = webClient.getOptions();
	// webClientOptions.setCssEnabled(false);
	// webClientOptions.setThrowExceptionOnFailingStatusCode(false);
	// webClientOptions.setThrowExceptionOnScriptError(false);
	// webClientOptions.setJavaScriptEnabled(true);
	// HtmlPage htmlPage = HTMLParser.parseHtml(page.getWebResponse(), webClient.getCurrentWindow());
	// List<HtmlElement> items = htmlPage.getByXPath("//img[@class, '_1Nyybr']");
	// if (items.isEmpty()) {
	// System.out.println("No items found !");
	// } else {
	// System.out.println("domelements=" + items.size());
	// for (HtmlElement htmlElement : items) {
	// System.out.println(htmlElement);
	// }
	// }
	//
	// }
}
