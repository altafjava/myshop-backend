package com.altaf.main.dto;

import java.util.List;
import com.altaf.scrap.entity.Phone;

public class PageableMobile {

	private Page page;
	private List<Phone> mobiles;

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<Phone> getMobiles() {
		return mobiles;
	}

	public void setMobiles(List<Phone> mobiles) {
		this.mobiles = mobiles;
	}

}
