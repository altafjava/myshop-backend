package com.altaf.scrap.enums;

public enum AvailabilityEnum {

	IN_STOCK, COMING_SOON, OUT_OF_STOCK
}
