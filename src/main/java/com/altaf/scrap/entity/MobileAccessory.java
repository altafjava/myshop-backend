package com.altaf.scrap.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

@Document
public class MobileAccessory {

	@Id
	private String id;
	@TextIndexed
	private String platform;
	private String productId;
	@TextIndexed
	private String productBrand;
	@TextIndexed(weight = 3)
	private String title;
	private double mrp;
	private double discountAmount;
	private double discountPercentage;
	private double finalPrice;
	private List<OlderPrice> olderPrices;
	private String currency;
	@TextIndexed(weight = 2)
	private List<String> keySpecList;
	private String titleImageLink;
	private List<String> imageUrlList;
	private String availability;
	private double userRating;
	private double baseRating;
	private int userRatingCount;
	private int userReviewCount;
	private int histogramBaseCount;
	private String warrantySummary;
	private String url;
	private String companyName;
	private String itemId;
	private String prevUrl;
	private String nextUrl;
	private int totalPages;
	@TextScore
	private Float score;
	private Date createdDate;
	private Date updatedDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getMrp() {
		return mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public List<OlderPrice> getOlderPrices() {
		return olderPrices;
	}

	public void setOlderPrices(List<OlderPrice> olderPrices) {
		this.olderPrices = olderPrices;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<String> getKeySpecList() {
		return keySpecList;
	}

	public void setKeySpecList(List<String> keySpecList) {
		this.keySpecList = keySpecList;
	}

	public String getTitleImageLink() {
		return titleImageLink;
	}

	public void setTitleImageLink(String titleImageLink) {
		this.titleImageLink = titleImageLink;
	}

	public List<String> getImageUrlList() {
		return imageUrlList;
	}

	public void setImageUrlList(List<String> imageUrlList) {
		this.imageUrlList = imageUrlList;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public double getUserRating() {
		return userRating;
	}

	public void setUserRating(double userRating) {
		this.userRating = userRating;
	}

	public double getBaseRating() {
		return baseRating;
	}

	public void setBaseRating(double baseRating) {
		this.baseRating = baseRating;
	}

	public int getUserRatingCount() {
		return userRatingCount;
	}

	public void setUserRatingCount(int userRatingCount) {
		this.userRatingCount = userRatingCount;
	}

	public int getUserReviewCount() {
		return userReviewCount;
	}

	public void setUserReviewCount(int userReviewCount) {
		this.userReviewCount = userReviewCount;
	}

	public int getHistogramBaseCount() {
		return histogramBaseCount;
	}

	public void setHistogramBaseCount(int histogramBaseCount) {
		this.histogramBaseCount = histogramBaseCount;
	}

	public String getWarrantySummary() {
		return warrantySummary;
	}

	public void setWarrantySummary(String warrantySummary) {
		this.warrantySummary = warrantySummary;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getPrevUrl() {
		return prevUrl;
	}

	public void setPrevUrl(String prevUrl) {
		this.prevUrl = prevUrl;
	}

	public String getNextUrl() {
		return nextUrl;
	}

	public void setNextUrl(String nextUrl) {
		this.nextUrl = nextUrl;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
