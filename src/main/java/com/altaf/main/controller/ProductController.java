package com.altaf.main.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.altaf.main.dto.PageableMobile;
import com.altaf.main.service.ProductService;
import com.altaf.scrap.entity.Mobile;
import com.altaf.scrap.entity.Phone;
import com.altaf.scrap.repository.MobileRepository;

@RestController
@CrossOrigin(origins = "*")
public class ProductController {

	@Autowired
	private ProductService productService;
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private MobileRepository mobileRepository;

	@GetMapping("/search")
	public PageableMobile searchProduct(@RequestParam("q") String searchString, @RequestParam int pageNo) {
		System.out.println("search query=" + searchString);
		return productService.searchProduct(searchString, pageNo);
	}

	// @GetMapping("/search")
	// public List<Mobile> searchProduct(@RequestParam("q") String searchString, @RequestParam int pageNo) {
	// System.out.println("search query=" + searchString);
	// List<Mobile> mobiles = new ArrayList<>();
	// String[] splits = searchString.split(" ");
	// TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matchingAny(splits);
	// Pageable pageable = PageRequest.of(pageNo, 10);
	// Page<Mobile> mobilePages = mobileRepository.findBy(textCriteria, pageable);
	// if (mobilePages.hasContent()) {
	// int totalPages = mobilePages.getTotalPages();
	// mobiles = mobilePages.getContent();
	// mobiles.forEach(mobile -> mobile.setTotalPages(totalPages));
	// }
	// mobiles = createMobiles();
	// return mobiles;
	// }

	private List<Mobile> createMobiles() {
		List<Mobile> mobiles = new ArrayList<>();
		Mobile mobile = new Mobile();
		mobile.setTitle("Realme C3 (Frozen Blue, 32 GB)  (3 GB RAM)");
		mobile.setMrp(25000.00);
		mobile.setDiscountAmount(1500.00);
		mobile.setFinalPrice(23500.00);
		mobile.setId("id1");
		mobile.setTitleImageLink("https://images-eu.ssl-images-amazon.com/images/I/31zlfKCyfXL._AC_UL160_SR160,160_.jpg");
		mobile.setKeySpecList(createKeySpecList());
		mobile.setUrl(
				"https://www.flipkart.com/realme-c3-frozen-blue-32-gb/p/itm58bf81a807d66?pid=MOBFZHC5HAGKGBBW&affid=sulakshanm&affExtParam1=electronics&affExtParam2=202003026397544");
		mobiles.add(mobile);

		mobile = new Mobile();
		mobile.setTitle("Redmi Note8 (Red 32 GB)");
		mobile.setMrp(14000.00);
		mobile.setDiscountAmount(1200.00);
		mobile.setFinalPrice(12800.00);
		mobile.setId("id2");
		mobile.setTitleImageLink("https://images-eu.ssl-images-amazon.com/images/I/41mGLH7EUNL._SL500_AC_SS350_.jpg");
		mobile.setKeySpecList(createKeySpecList());
		mobile.setUrl(
				"https://www.flipkart.com/realme-c3-frozen-blue-32-gb/p/itm58bf81a807d66?pid=MOBFZHC5HAGKGBBW&affid=sulakshanm&affExtParam1=electronics&affExtParam2=202003026397544");
		mobiles.add(mobile);

		mobile = new Mobile();
		mobile.setTitle("Oppp Reno3 (Gold 4gb)");
		mobile.setMrp(23000.00);
		mobile.setDiscountAmount(1600.00);
		mobile.setFinalPrice(22400.00);
		mobile.setId("id3");
		mobile.setTitleImageLink("https://images-eu.ssl-images-amazon.com/images/I/31kLWvXbGrL._SL500_AC_SS350_.jpg");
		mobile.setKeySpecList(createKeySpecList());
		mobile.setUrl(
				"https://www.flipkart.com/realme-c3-frozen-blue-32-gb/p/itm58bf81a807d66?pid=MOBFZHC5HAGKGBBW&affid=sulakshanm&affExtParam1=electronics&affExtParam2=202003026397544");
		mobiles.add(mobile);
		return mobiles;
	}

	private List<String> createKeySpecList() {
		List<String> keySpecList = new ArrayList<>();
		keySpecList.add("3 GB RAM | 32 GB ROM | Expandable Upto 256 GB");
		keySpecList.add("16.56 cm (6.52 inch) HD+ Display");
		keySpecList.add("12MP + 2MP | 5MP Front Camera");
		keySpecList.add("5000 mAh Battery");
		keySpecList.add("Helio G70 Processor");
		keySpecList.add("No cost EMI starting from ₹584/month");
		keySpecList.add("Cash on Delivery");
		keySpecList.add("Net banking & Credit/ Debit/ ATM card");
		return keySpecList;
	}
}
