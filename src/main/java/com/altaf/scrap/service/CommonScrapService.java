package com.altaf.scrap.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.altaf.scrap.entity.OlderPrice;
import com.altaf.scrap.entity.Phone;
import com.altaf.scrap.entity.PriceObject;
import com.altaf.scrap.enums.PlatformEnum;
import com.altaf.scrap.repository.PhoneRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

@Service
public class CommonScrapService {

	private static final Logger LOGGER = LogManager.getLogger(CommonScrapService.class);

	private String imageWidth = "312";
	private String imageHeight = "312";
	private String imageQuality = "70";
	@Autowired
	private PhoneRepository phoneRepository;

	public boolean scrapFlipkartMobile(Document document, String endpoint) {
		boolean isSuccessfullyCompleted = true;
		String targetHost = "https://flipkart.com";
		boolean isDataAvailable = false;
		Element mainElement = document.getElementById("is_script");
		Element pageSizeElement = document.getElementsByClass("_2zg3yZ").first();
		String pageSizeString = pageSizeElement.getElementsByTag("span").first().text();
		String[] splits = pageSizeString.split("of");
		int currentPage = Integer.parseInt(splits[0].substring(4).trim());
		int totalPages = Integer.parseInt(splits[1].trim());
		boolean isItFirstTime = true;
		do {
			LOGGER.info("isItFirstTime=" + isItFirstTime);
			if (!isItFirstTime) {
				try {
					document = Jsoup.connect(endpoint).timeout(60000).get();
					mainElement = document.getElementById("is_script");
				} catch (IOException e) {
					isSuccessfullyCompleted = false;
					LOGGER.error("Jsoup Connection Failed=" + e);
				}
			}
			isItFirstTime = false;
			isDataAvailable = false;
			String mainElementString = mainElement.html();
			String jsonString = mainElementString.substring(27, mainElementString.length() - 1);
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode rootNode = null;
			try {
				rootNode = objectMapper.readTree(jsonString);
			} catch (JsonProcessingException e) {
				isSuccessfullyCompleted = false;
				LOGGER.error("ObjectMapper Failed=" + e);
			}
			if (rootNode == null) {
				LOGGER.error("rootNode is null");
				LOGGER.error("Exiting from loop...");
				isSuccessfullyCompleted = false;
				break;
			}
			JsonNode pageDataNode = rootNode.path("pageDataV4");
			ArrayNode dataArrayNode = (ArrayNode) pageDataNode.path("page").path("data").get("10003");
			if (dataArrayNode.isArray()) {
				for (JsonNode jsonNode : dataArrayNode) {
					String slotType = jsonNode.get("slotType").textValue();
					if (slotType.equals("WIDGET")) {
						JsonNode widgetNode = jsonNode.get("widget");
						String type = widgetNode.get("type").textValue();
						if (type.equals("PRODUCT_SUMMARY")) {
							JsonNode productInfoNode = widgetNode.get("data").get("products").get(0).get("productInfo");
							JsonNode actionNode = productInfoNode.get("action");
							String url = targetHost + actionNode.get("url").textValue();
							JsonNode valueNode = productInfoNode.get("value");
							String availability = valueNode.get("availability").get("displayState").textValue();
							String productId = valueNode.get("id").textValue();

							Phone mobile = null;
							List<OlderPrice> olderPrices = null;
							Optional<Phone> existingPhone = phoneRepository.findByProductId(productId);
							Date currentDate = new Date();
							if (existingPhone.isPresent()) {
								mobile = existingPhone.get();
								olderPrices = mobile.getOlderPrices();
								mobile.setUpdatedDate(currentDate);
							} else {
								mobile = new Phone();
								mobile.setCreatedDate(currentDate);
								mobile.setUpdatedDate(currentDate);
								olderPrices = new ArrayList<>();
							}
							if (mobile.getCreatedDate() != mobile.getUpdatedDate()) {
								OlderPrice olderPrice = buildOlderPrice(mobile, currentDate);
								olderPrices.add(olderPrice);
								mobile.setOlderPrices(olderPrices);
							}
							mobile.setAvailability(availability);
							mobile.setUrl(url);
							mobile.setPlatform(PlatformEnum.FLIPKART.name());
							mobile.setProductId(productId);
							String itemId = valueNode.get("itemId").textValue();
							mobile.setItemId(itemId);
							ArrayNode keySpecsArrayNode = (ArrayNode) valueNode.get("keySpecs");
							List<String> keySpecList = new ArrayList<>();
							for (JsonNode keySpecNode : keySpecsArrayNode) {
								keySpecList.add(keySpecNode.textValue());
							}
							mobile.setKeySpecList(keySpecList);
							ArrayNode imagesArrayNode = (ArrayNode) valueNode.get("media").get("images");
							List<String> imageUrlList = new ArrayList<>();
							for (JsonNode imageNode : imagesArrayNode) {
								String imageUrl = imageNode.get("url").textValue().replace("{@width}", imageWidth).replace("{@height}", imageHeight)
										.replace("{@quality}", imageQuality);
								imageUrlList.add(imageUrl);
							}
							mobile.setImageUrlList(imageUrlList);
							mobile.setTitleImageLink(imageUrlList.get(0));
							JsonNode pricingNode = valueNode.get("pricing");
							int discountAmount = pricingNode.get("discountAmount").asInt();
							mobile.setDiscountAmount(discountAmount);
							JsonNode finalPriceNode = pricingNode.get("finalPrice");
							int finalPrice = finalPriceNode.get("decimalValue").asInt();
							mobile.setFinalPrice(finalPrice);
							String currency = finalPriceNode.get("currency").textValue();
							mobile.setCurrency(currency);
							int mrp = pricingNode.get("mrp").get("decimalValue").asInt();
							mobile.setMrp(mrp);
							int discountPercentage = pricingNode.get("totalDiscount").asInt();
							mobile.setDiscountPercentage(discountPercentage);
							String productBrand = valueNode.get("productBrand").textValue();
							mobile.setProductBrand(productBrand);
							JsonNode ratingNode = valueNode.get("rating");
							double userRating = ratingNode.get("average").asDouble();
							mobile.setUserRating(userRating);
							double baseRating = ratingNode.get("base").asDouble();
							mobile.setBaseRating(baseRating);
							int userRatingCount = ratingNode.get("count").asInt();
							mobile.setUserRatingCount(userRatingCount);
							int userReviewCount = ratingNode.get("reviewCount").asInt();
							mobile.setUserReviewCount(userReviewCount);
							int histogramBaseCount = ratingNode.get("histogramBaseCount").asInt();
							mobile.setHistogramBaseCount(histogramBaseCount);
							String title = valueNode.get("titles").get("title").textValue();
							mobile.setTitle(title);
							String warrantySummary = valueNode.get("warrantySummary").textValue();
							mobile.setWarrantySummary(warrantySummary);
							JsonNode seoPaginationNode = pageDataNode.get("browseMetadata").get("seoPagination");
							JsonNode prevUrlNode = seoPaginationNode.get("prevUrl");
							if (prevUrlNode != null) {
								String prevUrl = targetHost + prevUrlNode.textValue();
								mobile.setPrevUrl(prevUrl);
							}
							JsonNode nextUrlNode = seoPaginationNode.get("nextUrl");
							if (nextUrlNode != null) {
								String nextUrl = targetHost + nextUrlNode.textValue();
								mobile.setNextUrl(nextUrl);
								endpoint = nextUrl;
								LOGGER.info("endpoint=" + endpoint);
							}
							isDataAvailable = true;
							saveMobile(mobile);
						}
					}
				}
				LOGGER.info("currentPage=" + currentPage);
				currentPage++;
			}
		} while (currentPage <= totalPages && isDataAvailable);
		return isSuccessfullyCompleted;
	}

	private OlderPrice buildOlderPrice(Phone mobile, Date currentDate) {
		PriceObject priceObject = new PriceObject();
		priceObject.setMrp(mobile.getMrp());
		priceObject.setDiscountAmount(mobile.getDiscountAmount());
		priceObject.setFinalPrice(mobile.getFinalPrice());

		OlderPrice olderPrice = new OlderPrice();
		olderPrice.setDate(currentDate);
		olderPrice.setPriceObject(priceObject);
		return olderPrice;
	}

	private void saveMobile(Phone mobile) {
		phoneRepository.save(mobile);
	}

}
