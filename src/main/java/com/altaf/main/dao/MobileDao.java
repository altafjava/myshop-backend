package com.altaf.main.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import com.altaf.main.dto.Page;
import com.altaf.main.dto.PageableMobile;
import com.altaf.scrap.entity.Phone;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
public class MobileDao {

	@Value("${PER_PAGE_SIZE}")
	private int perPageSize;
	@Autowired
	private RestHighLevelClient restHighLevelClient;
	@Autowired
	private ObjectMapper objectMapper;

	public PageableMobile searchMobiles(String searchString, int currentPage) {
		SearchRequest searchRequest = buildsearchRequest(searchString, currentPage);
		SearchResponse searchResponse = null;
		try {
			searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PageableMobile pageableMobile = new PageableMobile();
		pageableMobile = getPageableMobile(pageableMobile, searchResponse, currentPage);
		return pageableMobile;
	}

	private SearchRequest buildsearchRequest(String searchString, int currentPage) {
		SearchRequest searchRequest = new SearchRequest();
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		int from = (currentPage - 1) * perPageSize;
		searchSourceBuilder.from(from);
		searchSourceBuilder.size(perPageSize);
		MultiMatchQueryBuilder multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(searchString, "title", "keySpecList").fuzziness(Fuzziness.ONE);
		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery().should(multiMatchQueryBuilder);
		searchSourceBuilder.query(boolQueryBuilder);
		searchRequest.source(searchSourceBuilder);
		searchRequest.indices("products.phone");
		return searchRequest;
	}
//	private SearchRequest buildsearchRequest(String searchString, int currentPage) {
//		SearchRequest searchRequest = new SearchRequest();
//		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//		int from = (currentPage - 1) * perPageSize;
//		searchSourceBuilder.from(from);
//		searchSourceBuilder.size(perPageSize);
//		searchSourceBuilder.query(QueryBuilders.matchQuery("title", searchString));
//		searchRequest.source(searchSourceBuilder);
//		searchRequest.indices("products.phone");
//		return searchRequest;
//	}

	private PageableMobile getPageableMobile(PageableMobile pageableMobile, SearchResponse searchResponse, int currentPage) {
		List<Phone> mobiles = new ArrayList<>();
		pageableMobile.setMobiles(mobiles);
		if (searchResponse != null && searchResponse.getHits().getTotalHits().value > 0) {
			int totalRecords = (int) searchResponse.getHits().getTotalHits().value;
			Page page = buildPage(totalRecords, currentPage);
			pageableMobile.setPage(page);
			for (SearchHit searchHit : searchResponse.getHits()) {
				Phone mobile = null;
				String responseString = searchHit.getSourceAsString();
				try {
					mobile = objectMapper.readValue(responseString, Phone.class);
					mobile.setId(searchHit.getId());
					mobiles.add(mobile);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("no results found");
		}
		return pageableMobile;
	}

	private Page buildPage(int totalRecords, int currentPage) {
		Page page = new Page();
		page.setTotalRecords(totalRecords);
		page.setPerPage(perPageSize);
		int totalPages = totalRecords / perPageSize;
		if (totalRecords % perPageSize != 0) {
			totalPages += 1;
		}
		page.setTotalPages(totalPages);
		page.setCurrentPage(currentPage);
		return page;
	}
}
