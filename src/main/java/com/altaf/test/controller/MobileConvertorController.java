package com.altaf.test.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.altaf.scrap.entity.Mobile;
import com.altaf.scrap.entity.MobileAccessory;
import com.altaf.scrap.entity.MobileBackup;
import com.altaf.scrap.entity.OlderPrice;
import com.altaf.scrap.entity.Phone;
import com.altaf.scrap.entity.PriceObject;
import com.altaf.scrap.enums.PlatformEnum;
import com.altaf.scrap.repository.MobileAccessoryRepository;
import com.altaf.scrap.repository.MobileBackupRepository;
import com.altaf.scrap.repository.MobileRepository;
import com.altaf.scrap.repository.PhoneRepository;
import com.altaf.scrap.util.AmazonUtil;

@RestController
public class MobileConvertorController {

	@Autowired
	private PhoneRepository phoneRepository;
	@Autowired
	private MobileAccessoryRepository mobileAccessoryRepository;
	@Autowired
	private MobileRepository mobileRepository;
	@Autowired
	private MobileBackupRepository mobileBackupRepository;

	@GetMapping("/update-phone")
	public String updatePhone() {
		Iterable<Phone> iterablePhone = phoneRepository.findAll();
		Iterator<Phone> iteratorPhone = iterablePhone.iterator();
		int count = 0;
		while (iteratorPhone.hasNext()) {
			count++;
			Phone phone = iteratorPhone.next();
			if (phone.getCompanyName() == null) {
				phone.setPlatform(PlatformEnum.FLIPKART.name());
				phone.setTitleImageLink(phone.getImageUrlList().get(0));
			} else {
				phone.setPlatform(PlatformEnum.AMAZON.name());
				String url = phone.getUrl();
				int index = url.indexOf("/dp/") + 4;
				String productId = url.substring(index, index + 10);
				phone.setProductId(productId);
			}

			PriceObject priceObject = new PriceObject();
			priceObject.setMrp(phone.getMrp());
			priceObject.setDiscountAmount(phone.getDiscountAmount());
			priceObject.setFinalPrice(phone.getFinalPrice());

			OlderPrice olderPrice = new OlderPrice();
			olderPrice.setDate(new Date());
			olderPrice.setPriceObject(priceObject);

			List<OlderPrice> olderPrices = phone.getOlderPrices() == null ? new ArrayList<>() : phone.getOlderPrices();
			olderPrices.add(olderPrice);
			phone.setOlderPrices(olderPrices);
			phoneRepository.save(phone);
		}
		return "Phone Updated = " + count;
	}

	@GetMapping("/mobile-backup")
	public String mobileBackup() {
		Iterable<Mobile> iterableMobile = mobileRepository.findAll();
		Iterator<Mobile> iteratorMobile = iterableMobile.iterator();
		int count = 0;
		while (iteratorMobile.hasNext()) {
			count++;
			Mobile mobile = iteratorMobile.next();
			MobileBackup mobileBackup = new MobileBackup();
			BeanUtils.copyProperties(mobile, mobileBackup);
			mobileBackupRepository.save(mobileBackup);
		}
		return "MobileBackup saved = " + count;
	}

	@GetMapping("/phone-mobile")
	public String convertPhoneToMobile() {
		Iterable<Phone> iterablePhone = phoneRepository.findAll();
		Iterator<Phone> iteratorPhone = iterablePhone.iterator();
		int count = 0;
		while (iteratorPhone.hasNext()) {
			count++;
			Phone phone = iteratorPhone.next();
			Mobile mobile = new Mobile();
			BeanUtils.copyProperties(phone, mobile);
			mobileRepository.save(mobile);
		}
		return "Mobile Saved = " + count;
	}

	@GetMapping("/mobile-to-phone-accessory")
	public String test() {
		Iterable<Mobile> iterableMobile = mobileRepository.findAll();
		Iterator<Mobile> iteratorMobile = iterableMobile.iterator();
		int count = 0;
		int mobileAccessoryCount = 0;
		int phoneCount = 0;
		while (iteratorMobile.hasNext()) {
			count++;
			Mobile mobile = iteratorMobile.next();
			String title = mobile.getTitle().toLowerCase();
			if (AmazonUtil.isMobileAccesory(title, mobile.getFinalPrice())) {
				mobileAccessoryCount++;
				MobileAccessory mobileAccessory = new MobileAccessory();
				BeanUtils.copyProperties(mobile, mobileAccessory);
				mobileAccessoryRepository.save(mobileAccessory);
			} else {
				phoneCount++;
				Phone phone = new Phone();
				BeanUtils.copyProperties(mobile, phone);
				phoneRepository.save(phone);
			}
		}
		return "Total Count=" + count + "   PhoneCount=" + phoneCount + "   mobileAccessoryCount=" + mobileAccessoryCount;
	}

}
