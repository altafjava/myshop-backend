package com.altaf.scrap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class JsoupTest {
	// minimum height/width=10/10
	// maximum height/width=4499/4499
	// q=1-100
	public static void main(String[] args) throws IOException {
		String imageWidth = "312";
		String imageHeight = "312";
		String imageQuality = "70";
		String targetHost = "https://flipkart.com";
		boolean isDataAvailable = false;
		System.out.println("starting...\n");
		String endpoint = "https://www.flipkart.com/mobiles/mi~brand/pr?sid=tyy%2C4io";
		System.out.println(endpoint);
		Document document = Jsoup.connect(endpoint).timeout(60000).get();
		Element mainElement = document.getElementById("is_script");
		Element pageSizeElement = document.getElementsByClass("_2zg3yZ").first();
		String pageSizeString = pageSizeElement.getElementsByTag("span").first().text();
		String[] splits = pageSizeString.split("of");
		int currentPage = Integer.parseInt(splits[0].substring(4).trim());
		int totalPages = Integer.parseInt(splits[1].trim());
		boolean isItFirstTime = true;
		do {
			if (!isItFirstTime) {
				document = Jsoup.connect(endpoint).timeout(60000).get();
				mainElement = document.getElementById("is_script");
			}
			isItFirstTime = false;
			isDataAvailable = false;
			String mainElementString = mainElement.html();
			String jsonString = mainElementString.substring(27, mainElementString.length() - 1);
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode rootNode = objectMapper.readTree(jsonString);
			JsonNode pageDataNode = rootNode.path("pageDataV4");
			ArrayNode dataArrayNode = (ArrayNode) pageDataNode.path("page").path("data").get("10003");
			if (dataArrayNode.isArray()) {
				for (JsonNode jsonNode : dataArrayNode) {
					String slotType = jsonNode.get("slotType").textValue();
					if (slotType.equals("WIDGET")) {
						JsonNode widgetNode = jsonNode.get("widget");
						String type = widgetNode.get("type").textValue();
						if (type.equals("PRODUCT_SUMMARY")) {
							JsonNode productInfoNode = widgetNode.get("data").get("products").get(0).get("productInfo");
							JsonNode actionNode = productInfoNode.get("action");
							// if (actionNode != null) {
							String url = "https://www.flipkart.com" + actionNode.get("url").textValue();
							System.out.println(url);
							JsonNode valueNode = productInfoNode.get("value");
							String availability = valueNode.get("availability").get("displayState").textValue();
							System.out.println(availability);
							String productId = valueNode.get("id").textValue();
							String itemId = valueNode.get("itemId").textValue();
							System.out.println(productId + "   " + itemId);
							ArrayNode keySpecsArrayNode = (ArrayNode) valueNode.get("keySpecs");
							List<String> keySpecList = new ArrayList<>();
							for (JsonNode keySpecNode : keySpecsArrayNode) {
								keySpecList.add(keySpecNode.textValue());
								System.out.println(keySpecNode.textValue());
							}
							ArrayNode imagesArrayNode = (ArrayNode) valueNode.get("media").get("images");
							List<String> imageUrlList = new ArrayList<>();
							for (JsonNode imageNode : imagesArrayNode) {
								String imageUrl = imageNode.get("url").textValue().replace("{@width}", imageWidth).replace("{@height}", imageHeight)
										.replace("{@quality}", imageQuality);
								imageUrlList.add(imageUrl);
								System.out.println(imageUrl);
							}
							JsonNode pricingNode = valueNode.get("pricing");
							int discountAmount = pricingNode.get("discountAmount").asInt();
							JsonNode finalPriceNode = pricingNode.get("finalPrice");
							int finalPrice = finalPriceNode.get("decimalValue").asInt();
							System.out.println("finalPrice=" + finalPrice);
							String currency = finalPriceNode.get("currency").textValue();
							int mrp = pricingNode.get("mrp").get("decimalValue").asInt();
							int totalDiscount = pricingNode.get("totalDiscount").asInt();
							System.out.println(mrp + " - " + discountAmount + " " + totalDiscount + "%  = " + finalPrice + " " + currency);
							JsonNode productBrandNode = valueNode.get("productBrand");
							System.out.println(productBrandNode);
							JsonNode ratingNode = valueNode.get("rating");
							double userRating = ratingNode.get("average").asDouble();
							double baseRating = ratingNode.get("base").asDouble();
							int userRatingCount = ratingNode.get("count").asInt();
							int userReviewCount = ratingNode.get("reviewCount").asInt();
							int histogramBaseCount = ratingNode.get("histogramBaseCount").asInt();
							System.out.println(userRating + "  " + baseRating + "  " + userRatingCount + "  " + userReviewCount + "  " + histogramBaseCount);
							String title = valueNode.get("titles").get("title").textValue();
							System.out.println(title);
							String warrantySummary = valueNode.get("warrantySummary").textValue();
							System.out.println(warrantySummary);
							JsonNode seoPaginationNode = pageDataNode.get("browseMetadata").get("seoPagination");
							JsonNode prevUrlNode = seoPaginationNode.get("prevUrl");
							if (prevUrlNode != null) {
								String prevUrl = targetHost + prevUrlNode.textValue();
								System.out.println(prevUrl);
							}
							JsonNode nextUrlNode = seoPaginationNode.get("nextUrl");
							if (nextUrlNode != null) {
								String nextUrl = targetHost + nextUrlNode.textValue();
								endpoint = nextUrl;
								System.out.println(nextUrl);
							}
							isDataAvailable = true;
						}
					}
				}
				System.out.println(currentPage + " Page --------------------------\n");
				currentPage++;
			}
		} while (currentPage <= totalPages && isDataAvailable);
		System.out.println("--------------------DONE -----------------");
	}

}
