package com.altaf.scrap.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.altaf.scrap.entity.Mobile;

@Repository
// public interface MobileRepository extends MongoRepository<Mobile, String> {
public interface MobileRepository extends CrudRepository<Mobile, String> {

	Page<Mobile> findBy(TextCriteria textCriteria, Pageable pageable);

	// find all matching documents and sort by relevance
	List<Mobile> findAllByOrderByScoreDesc(TextCriteria criteria);
}
