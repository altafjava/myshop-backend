package com.altaf.scrap.flipkart.mobile;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MiMobileScrapper {

	static int count = 0;

	public static void main(String[] args) throws IOException {
		String baseUrl = "https://www.flipkart.com/mobiles/mi~brand/pr?sid=tyy%2C4io&page=";
		String url = baseUrl + 1;
		Document document = Jsoup.connect(url).get();
		Elements pageElement = document.getElementsByClass("_2zg3yZ");
		Elements spanElement = pageElement.get(0).getElementsByTag("span");
		String page = spanElement.text();
		int min = Integer.parseInt(page.substring(5, page.indexOf("of")).trim());
		int max = Integer.parseInt(page.substring(page.indexOf("of") + 2, page.indexOf("Next")).trim());
		System.out.println("min=" + min + "   max=" + max);
		getBox(document);
		for (int i = min + 1; i <= max; i++) {
			System.out.println("i=" + i);
			url = baseUrl + i;
			document = Jsoup.connect(url).get();
			getBox(document);
		}
		System.err.println("count=" + count);
	}

	public static void getBox(Document document) {
		Elements elements = document.getElementsByClass("col-7-12");
		for (Element element : elements) {
			String title = element.getElementsByClass("_3wU53n").text();
			System.out.println(title);
			Elements properties = element.getElementsByClass("tVe95H");
			for (Element propertyElement : properties) {
				System.out.println(propertyElement.text());
			}
			System.out.println();
			count++;
		}
	}
}
