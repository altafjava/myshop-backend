package com.altaf.scrap.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.altaf.scrap.entity.Phone;

@Repository
public interface PhoneRepository extends CrudRepository<Phone, String> {

	Page<Phone> findBy(TextCriteria textCriteria, Pageable pageable);

	// find all matching documents and sort by relevance
	List<Phone> findAllByOrderByScoreDesc(TextCriteria criteria);

	Optional<Phone> findByProductId(String productId);
}
